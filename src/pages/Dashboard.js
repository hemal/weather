/*******************************************************************************
 *
 * Dashboard.js
 * Main view of the weather app after the location has been defined
 *
 * Created by vhn on 2019-07-16.
 /*****************************************************************************/

import React from 'react';

function Dashboard() {
  return(
      <div>
        <h1>Dashboard</h1>
      </div>
  )
}

export default Dashboard;
