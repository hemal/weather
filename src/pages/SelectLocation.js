/*******************************************************************************
 *
 * SelectLocation.js
 * SelectLocation allows the user to define a specific city or automatically
 * use a location
 *
 * Created by vhn on 2019-07-16.
 /*****************************************************************************/

import React, { Component } from 'react'
import '../css/main.css'
import { Link, withRouter } from 'react-router-dom';
import MaterialIcon from 'material-icons-react';
import axios from 'axios';

import weatherAPI from '../config/weatherAPI'


class SelectLocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      location: '',
      query: ''
    }
  }

  getLocationName = (q) => {
    axios.get( weatherAPI[0].weatherURL+'/locations/v1/cities/autocomplete',
         {params: {
              apikey: weatherAPI[0].weatherApiKey,
              q: q
           }})
        .then( response =>
        {
          return(
              <option>{response.data}</option>
          )
        })
        .catch( error => {
          alert(error)
        })
    // this.props.history.push('/dashboard')
  };

  render() {
    return (
        <div className="SearchBox">
          <div className="searchControl">
            <input className="citySearch" name="city" placeholder="city" onChange={ (q) => { this.getLocationName(q.target.value)}}/>

            <button className="searchButton" onClick={ this.getLocationName}>
              <MaterialIcon icon="search" size='large' />
            </button>
          </div>

          <h4>or</h4>
          <h3>use my current location</h3>
        </div>
    )
  }
}

export default withRouter(SelectLocation);
