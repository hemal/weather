/*******************************************************************************
 *
 * MainLayout.js
 * MainLayout is
 *
 * Created by vhn on 2019-07-17.
 /*****************************************************************************/

import React from 'react';
import SelectLocation from '../pages/SelectLocation';
import Dashboard from '../pages/Dashboard';
import { Route, Switch } from 'react-router-dom';

function MainLayout() {
  return(
      // MainView
      <div className="mainContainer" >

        {/*Select location search box*/}
        <div className="contentBox">
          <Switch>
            <Route path="/location" exact={true} component={ SelectLocation }/>
            <Route path='/' component={ Dashboard }/>
          </Switch>
        </div>
      </div>
  )
}

export default MainLayout
